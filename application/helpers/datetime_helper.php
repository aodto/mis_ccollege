<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DateTimeHelper{
	public static function getNow($format='Y-m-d H:i:s')
	{
		$tz_string 			= "Asia/Bangkok";
		$tz_object 			= new DateTimeZone($tz_string);
		$datetime 			= new DateTime();
		$datetime->setTimezone($tz_object);
	
		return $datetime->format($format) ;
	}
}
?>