<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<!DOCTYPE html>
<html lang="en">
<?php echo $header;?>
<style>
.custom-table {
	margin-bottom: 0px !important;
}

.table td {
	border-top: 0px;
}

.col-1 {
	width: 50px;
}

.col-2 {
	width: 150px;
}

.col-3 {
	width: 150px;
}

.col-4 {
	width: 150px;
}

.col-5 {
	width: 150px;
}

.col-6 {
	width: 150px;
}

.col-7 {
	
}
</style>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<table class="table custom-table">
					<tr>
						<td class="col-1">ลำดับ</td>
						<td class="col-2">รหัสนักศึกษา</td>
						<td class="col-3">ชื่อ สกุล</td>
						<td class="col-4">คะแนนเก็บ</td>
						<td class="col-5">คะแนนสอบ</td>
						<td class="col-6">เกรด</td>
						<td class="col-7">สถานะ</td>
					</tr>
				</table>
			</div>
		</div>
	</div>


	<div class="container-fluid">

		<div class="accordion" id="accordion2">
			<?php 
			for($i=0; $i<60; $i++){
			?>
			<div class="accordion-group">
				<div class="accordion-heading">
					<a class="accordion-toggle" data-toggle="collapse"
						data-parent="#accordion2" href="#collapse-<?php echo $i;?>">
						<table class="table custom-table">
							<tr>
								<td class="col-1">#<?php echo ($i+1);?>
								</td>
								<td class="col-2">5531040023</td>
								<td class="col-3">นายปรัชญา ตั้งใจ</td>
								<td class="col-4">60</td>
								<td class="col-5">40</td>
								<td class="col-6">A</td>
								<td class="col-7">ปกติ</td>
							</tr>
						</table>
					</a>
				</div>
				<div id="collapse-<?php echo $i;?>"
					class="accordion-body collapse <?php echo ($i==0)?"in":"";?>">
					<div class="accordion-inner">

						<div class="row-fluid">
							<div class="span3">
								<div>
									<h5>คะแนนเก็บ (34/60) %</h5>
								</div>
								<div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 1"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 2"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 3"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 4"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 5"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 6"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 7"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 8"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 9"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 10"></div>
									<a href="#" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>
									<a href="#" class="btn btn-success"><i class="icon-plus icon-white"></i></a>
								</div>
							</div>
							<div class="span3">
								<div>
									<h5>คะแนนคุณธรรม (15/20) %</h5>
								</div>
								<div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 1"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 2"></div>
									<a href="#" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>
									<a href="#" class="btn btn-success"><i class="icon-plus icon-white"></i></a>
								</div>
							</div>
							<div class="span3">
								<div>
									<h5>คะแนนสอบ (15/20) %</h5>
								</div>
								<div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 1"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 2"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 3"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 4"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 5"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 6"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 7"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 8"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 9"></div>
									<div><input type="text" style="width: 120px;" placeholder="คะแนนช่องที่ 10"></div>
									<a href="#" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>
									<a href="#" class="btn btn-success"><i class="icon-plus icon-white"></i></a>
								</div>
							</div>
							<div class="span3">
								<div>
									<h5>สรุปคะแนน</h5>
								</div>
								<div>
									<div>
										<input type="text" value="5.6" class="span4" disabled> คะแนนเก็บเฉลี่ย
									</div>
									<div>
										<input type="text" value="5.6" class="span4" disabled> คะแนนสอบเฉลี่ย 
									</div>
									<div>
										<input type="text" value="4" class="span4" disabled> เกรด 
									</div>
									<div>
										<select name="txtscore_status1" style="width: 90px;">
												<option value="0">-ปกติ-</option>

												<option value="901">[ขร]</option>
												<option value="902">[ขส]</option>
												<option value="903">[ถล]</option>
												<option value="904">[ถน]</option>
												<option value="906">[ท]</option>
												<option value="905">[ถพ]</option>
												<option value="907">[มส]</option>
												<option value="908">[มท]</option>
												<option value="909">[มก]</option>
												<option value="910">[มผ]</option>
												<option value="911">[ผ]</option>
												<option value="912">[ขป]</option>
												<option value="913">[โอน]</option>
												<option value="914">[เรียนซ้ำ]</option>
												<option value="915">[ปรับพื้น]</option>
												<option value="916">[เทียบโอน]</option>
												<option value="917">[ฝึกงาน]</option>
												<option value="0">[ปกติ]</option>
										</select> สถานะ
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<?php 
			}
			?>
		</div>

	</div>
	<!--/.fluid-container-->
	<?php echo $footer;?>
</body>
</html>
