<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="well sidebar-nav">
	<ul class="nav nav-list">
	
		<li class="nav-header"><h5>บันทึกข้อมูลเบื้องต้นรวม</h5></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ส่งข้อความถึงนักศึกษา</a></li>
		<li class="active"><a href="#"><i class="icon-chevron-right"></i>[รายงานการเข้าใช้งาน]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>[รายงานการเข้าใช้งาน 2]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>[รายงานการเข้าใช้งาน 3]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>[รายงานการเข้าใช้งาน e-office]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>[รายงานการเข้าใช้งาน แผนก]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>[ข้อมูลขาดเรียนเกิน 20 %]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายชื่อกลุ่มนักศึกษา</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>จำนวนนักศึกษา[แผนก]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>จำนวนนักศึกษา[กลุ่ม]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายวิชาที่เปิดสอน</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลแผนการเรียน</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานรายชื่อครูที่ปรึกษาและนักเรียนในที่ปรึกษา</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ข้อมูลตัวบ่งชี้</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>กิจกรรมตัวบ่งชี้</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ตารางวิเคราะห์ระบบงาน</a></li>
		
		<li class="nav-header"><h5>มุมบันเทิง</h5></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ดูหนัง/ฟังเพลง/สารคดี</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>pbtc_TV online</a></li>
		
		<li class="nav-header"><h5>บันทึกข้อมูลการเรียนการสอน</h5></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกกิจกรรมด้วย Tablet</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกกิจกรรมด้วย Tablet2</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกกิจกรรมหน้าเสาธง</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกกิจกรรมโฮมรูม</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลกิจกรรมพิเศษ [กลุ่ม]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลยานพาหนะ[กลุ่ม]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลยานพาหนะ</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประวัตินักศึกษาในที่ปรึกษา</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลวิชาที่สอน[1]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลคะแนน/เวลาเรียน[2]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลเรียนซ้ำเรียนปรับ[3]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ตรวจสอบคะแนน/เวลาเรียนในที่ปรึกษา[4]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลขาดเรียนครูผู้สอน[5]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลขาดเรียนครูผู้สอน[ขร]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>การพิจารณาเวลาเรียน[กลุ่ม][6]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลขาดเรียนในที่ปรึกษา[7]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>[รายงานข้อมูลขาดเรียนเกิน 20 %]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>แจ้งขาดเรียนในที่ปรึกษา[sms]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>แจ้งขาดกิจกรรมหน้าเสาธง[sms]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลหน่วยวิชาที่สอน</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลโครงการสอน</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกคลังข้อสอบ</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลตรวจความเรียบร้อย</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลกิจกรรมหน้าเสาธง</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลกิจกรรมวิทยาลัย</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลกิจกรรมแผนก</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานการตรวจความเรียบร้อย</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลผิดระเบียบ</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานประเมินการเรียนการสอน/ครุภัณฑ์</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลบทความ[km]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลงานวิจัย</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลกิจกรรมส่วนตัว</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลโครงการ</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานผลโครงการ</a></li>
		
		<li class="nav-header"><h5>บันทึกข้อมูลประกันคุณภาพ</h5></li>
		<li><a href="#"><i class="icon-chevron-right"></i>บันทึกข้อมูลการอบรมสัมมนา</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมิน[ตัวบ่งชี้ 15]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมิน[ตัวบ่งชี้ 20]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมิน[ตัวบ่งชี้ 39]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมิน[ตัวบ่งชี้ 40]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมิน[ตัวบ่งชี้ 41]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมินระบบงาน</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมินอาคารเรียน</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมินอาคารเรียน</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมินอาคารเรียน</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ผลการประเมินระบบสารสนเทศ</a></li>
		
		<li class="nav-header"><h5>ประเมินการเรียนการสอน</h5></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประเมินระบบงานในวิทยาลัยฯ</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประเมินระบบสารสนเทศ</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประเมินอาคารเรียน[1]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประเมินอาคารเรียน[2]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประเมินอาคารเรียน[3]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประเมินตัวบ่งชี้ที่ 39</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประเมินตัวบ่งชี้ที่ 40</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>ประเมินตัวบ่งชี้ที่ 41</a></li>
		
		<li class="nav-header"><h5>รายงานกิจกรรมนักศึกษา</h5></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลการใช้งานห้องสมุด</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลการใช้งานศูนย์การเรียนรู้</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลการใช้ห้องคอมพิวเตอร์ [1]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลการใช้ห้องคอมพิวเตอร์ [2]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานข้อมูลการใช้ห้องคอมพิวเตอร์ [3]</a></li>
		<li><a href="#"><i class="icon-chevron-right"></i>รายงานรายชื่อครูที่ปรึกษาและนักเรียนในที่ปรึกษา</a></li>
		
		<li class="nav-header"><h5>เกี่ยวกับโปรแกรม</h5></li>
	</ul>
</div>

