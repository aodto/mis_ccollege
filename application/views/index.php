<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<?php echo $header;?>
<body>
	<div class="container-fluid">

		<div id="wrapper">

			<table class="table table-condensed table-hover">
				<thead>
					<tr>
						<th>ลำดับ</th>
						<th>รหัสกลุ่ม</th>
						<th style="width: 80px;">รหัสวิชา</th>
						<th>ชื่อวิชาที่สอน</th>
						<th>หน่วยกิต</th>
						<th>ชั่วโมงเรียน</th>
						<th>ปีการศึกษา</th>
						<th>โครงการสอน</th>
						<th>บันทึกคะแนน</th>
						<th>บันทึกเวลาเรียน</th>
						<th>รายชื่อ</th>
						<th>แจ้งขาดเรียน</th>
						<th>ใบส่งคะแนน</th>
						<th>ส่งผลการเรียน</th>
					</tr>
				</thead>
				<tbody>

					<?php 
					for($i=0; $i<count($items); $i++){
						$row = $items[$i];
					?>
					<tr>
						<td><div align="center"><span >1</span></div></td>
						<td><div align="center"><?php echo $row->code;?></div></td>
						<td><div align="center"><?php echo $row->subject_code;?></div></td>
						<td><div align="left"><?php echo $row->subject_title;?></div></td>
						<td><div align="center"><?php echo $row->amount_credit;?></div></td>
						<td><div align="center"><?php echo $row->amount_hours;?></div></td>
						<td><div align="center">&nbsp;1/2556</div></td>
						<td>
							<a class="btn" target="_bank" href="#"><i class="icon-print"></i> พิมพ์ส่งคะแนน</a>
						</td>
						<td>
							<a class="btn" href="<?php echo base_url("home/edit_score?form_id={$save_score_form_id}&subject_id={$row->subject_id}&group_id={$row->group_id}");?>"><i class="icon-pencil"></i> บันทึกคะแนน</a>
						</td>
						<td>
							<a class="btn" href="#"><i class="icon-time"></i> บันทึกเวลาเรียน </a>
						</td>
						<td>
							<a class="btn" target="_bank" href="#"><i class="icon-print"></i> พิมพ์รายชื่อ </a>
						</td>
						<td>
							<a class="btn" target="_bank" href="#"><i class="icon-print"></i> พิมพ์แจ้งขาดเรียน </a>
						</td>
						<td>
							<a class="btn" target="_bank" href="#"><i class="icon-print"></i> พิมพ์ใบส่งคะแนน </a>
						</td>
						<td>
							<a class="btn" href="#"><i class="icon-ok"></i> ส่งผลการเรียน</a>
						</td>
					</tr>
					<?php 
					}
					?>

				</tbody>
			</table>

		</div>

	</div>

	<!--/.fluid-container-->
	<?php echo $footer;?>
</body>
</html>
