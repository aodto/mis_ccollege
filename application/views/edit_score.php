<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<?php echo $header;?>
<body>
	<div class="container-fluid">

		<div id="wrapper">
		
			<?php 
			if($this->session->flashdata('status')!=""){
				?>
				<div class="alert alert-success fade in">
		            <button type="button" class="close" data-dismiss="alert">×</button>
		            <div style="text-align: center;"><?php echo $this->session->flashdata('status');?></div>
		            <div style="text-align: center; margin-top: 20px;">
		            	<a class="btn" href="<?php echo base_url("");?>"><i class="icon-arrow-left"></i> กลับหน้าหลัก</a>
		            </div>
		          </div>
				<?php
			}
			?>
		
			<form action="<?php echo base_url("home/save_score");?>" method="post">
		
				<div class="items">
					<div class="table-header">
						<div class="item">
							<div class="box ordering">
								<span>#</span>
							</div>
							<div class="box student_id">
								<span>รหัสนักศึกษา</span>
							</div>
							<div class="box student_name">
								<span>ชื่อ-สกุล</span>
							</div>
							<div class="box score_1">
								<span>คะแนนเก็บ </span>
								<a href="#" class="btn btn-mini" id="btn_score_1_remove"><i class="icon-minus"></i></a>
								<a href="#" class="btn btn-mini" id="btn_score_1_add"><i class="icon-plus"></i></a>
							</div>
							<div class="box box-sum score_1_sum">รวม</div>
							<div class="box score_2">
								<span>คะแนนคุณธรรม </span>
								<a href="#" class="btn btn-mini" id="btn_score_2_remove"><i class="icon-minus"></i></a>
								<a href="#" class="btn btn-mini" id="btn_score_2_add"><i class="icon-plus"></i></a>
							</div>
							<div class="box box-sum score_2_sum">รวม</div>
							<div class="box score_3">
								<span>คะแนนสอบ </span>
								<a href="#" class="btn btn-mini" id="btn_score_3_remove"><i class="icon-minus"></i></a>
								<a href="#" class="btn btn-mini" id="btn_score_3_add"><i class="icon-plus"></i></a>
							</div>
							<div class="box box-sum score_3_sum">รวม</div>
							<div class="box box-grade">เกรด</div>
							<div class="box box-status">สถานะ</div>
						</div>
					</div>
					<div class="table-content">
						<?php 
						for($i=0; $i<count($items_student); $i++){
							$row = $items_student[$i];
							
							if(isset($items_score[$i])) $val1 = json_decode($items_score[$i]->score_1);
							else $val1 = $init_score_1;
							
							if(isset($items_score[$i])) $val2 = json_decode($items_score[$i]->score_2);
							else $val2 = $init_score_2;
							
							if(isset($items_score[$i])) $val3 = json_decode($items_score[$i]->score_3);
							else $val3 = $init_score_3;
							
							//print_r($val1);
						?>
						<div class="item" id="row_<?php echo $i;?>" data-id="std<?php echo $row->student_id;?>">
							<div class="box ordering">
								<?php echo $i+1;?>
							</div>
							<div class="box student_id"><?php echo $row->student_code;?></div>
							<div class="box student_name"><?php echo $row->firstname." ".$row->lastname;?></div>
							<div class="box score_1">
								<?php 
								for($j=0; $j<count($val1); $j++){
								?>
								<input type="text" class="inputbox" style="width: 20px;" value="<?php echo $val1[$j];?>" name="n_score[<?php echo $i;?>][0][<?php echo $j;?>]" title="" data-placement="top" data-original-title="คะแนนเก็บ (<?php echo $init_percent_1;?>%)"> 
								<?php
								}
								?>
							</div>
							<div class="box box-sum score_1_sum" id="sum_score_<?php echo $i;?>_1">0/0</div>
							<div class="box score_2">
								<?php 
								for($j=0; $j<count($val2); $j++){
								?>
								<input type="text" class="inputbox" style="width: 20px;" value="<?php echo $val2[$j];?>" name="n_score[<?php echo $i;?>][1][<?php echo $j;?>]" title="" data-placement="top" data-original-title="คะแนนคุณธรรม (<?php echo $init_percent_2;?>%)"> 
								<?php
								}
								?>
							</div>
							<div class="box box-sum  score_2_sum" id="sum_score_<?php echo $i;?>_2">0/0</div>
							<div class="box score_3">
								<?php 
								for($j=0; $j<count($val3); $j++){
								?>
								<input type="text" class="inputbox" style="width: 20px;" value="<?php echo $val3[$j];?>" name="n_score[<?php echo $i;?>][2][<?php echo $j;?>]" title="" data-placement="top" data-original-title="คะแนนสอบ (<?php echo $init_percent_3;?>%)"> 
								<?php
								}
								?>
							</div>
							<div class="box box-sum  score_3_sum" id="sum_score_<?php echo $i;?>_3">0/0</div>
							<div class="box box-grade" id="grade_score_<?php echo $i;?>">-</div>
							<div class="box box-status">
								<select name="score_status[<?php echo $i;?>]" style="width: 100px;">
				                    <?php 
				                    for($n=0; $n<count($subject_status); $n++){
										$status = 0;
										if(isset($items_score[$i]->status)){
											$status = $items_score[$i]->status;
										}
				                    	?>
				                    	<option value="<?php echo $subject_status[$n]->id;?>" <?php echo ($status==$subject_status[$n]->id)?'selected':'';?>><?php echo $subject_status[$n]->title;?></option>
				                    	<?php
				                    }
				                    ?>
				                  </select>
							</div>
							<div style="display: none;"><input type="hidden" name="student_ids[<?php echo $i;?>]" value="<?php echo $row->student_id;?>"/></div>
						</div>
						<?php 
						}
						?>
					</div>
				</div>
				
				<div style="text-align: center; padding: 10px;">
					<button type="submit" class="btn btn-large btn-primary" style="width: 200px;"><i class="icon-share-alt icon-white"></i> บันทึกคะแนน</button>
					<div style="margin-top: 20px; "><a href="<?php echo base_url("");?>">กลับหน้าหลัก</a></div>
				</div>
				
				<input type="hidden" name="subject_id" value="<?php echo $subject_id;?>"/>
				<input type="hidden" name="form_id" value="<?php echo $form_id;?>"/>
				<input type="hidden" name="group_id" value="<?php echo $group_id;?>"/>
			</form>
			
		</div>

	</div>
	
	<!--/.fluid-container-->
	<?php echo $footer;?>
	<script>
		var minScore1 = <?php echo $init_min_colomn;?>;
		var minScore2 = <?php echo $init_min_colomn;?>;
		var minScore3 = <?php echo $init_min_colomn;?>;
		//nothing to implement max limited
		var maxScore1 = <?php echo $init_max_colomn;?>;
		var maxScore2 = <?php echo $init_max_colomn;?>;
		var maxScore3 = <?php echo $init_max_colomn;?>;

		var percent1 = <?php echo $init_percent_1;?>;
		var percent2 = <?php echo $init_percent_2;?>;
		var percent3 = <?php echo $init_percent_3;?>;

		function trace(msg){
			console.log(msg);
		}

		//hightlight row
		function active_row(target){
			//clear active status
			$( ".table-content .item" ).each(function( index ) {
				$(this).removeClass("active");
			});
			$(target).parent().parent().addClass("active");
			$(target).tooltip('show');
		}

		//calculate score
		function summary_score(){
			$( ".table-content .item" ).each(function( index ) {
				var sum_score1 = 0;
				$( "#row_"+index+" .score_1 input" ).each(function( index_box ) {
					var v = parseInt($(this).val());
					if(isNaN(v)) v = 0;
					sum_score1 += v;
				});

				var sum_score2 = 0;
				$( "#row_"+index+" .score_2 input" ).each(function( index_box ) {
					var v = parseInt($(this).val());
					if(isNaN(v)) v = 0;
					sum_score2 += v;
				});

				var sum_score3 = 0;
				$( "#row_"+index+" .score_3 input" ).each(function( index_box ) {
					var v = parseInt($(this).val());
					if(isNaN(v)) v = 0;
					sum_score3 += v;
				});
				
				//update summary score
				$("#sum_score_"+index+"_1").html(sum_score1+"/"+percent1);
				$("#sum_score_"+index+"_2").html(sum_score2+"/"+percent2);
				$("#sum_score_"+index+"_3").html(sum_score3+"/"+percent3);

				var grade_score = "-";
				var total_score = sum_score1+sum_score2+sum_score3;
				if(total_score>=80){
					grade_score = 4;
				}else if(total_score>=70 && total_score<80){
					grade_score = 3;
				}else if(total_score>=60 && total_score<70){
					grade_score = 2;
				}else if(total_score>=50 && total_score<60){
					grade_score = 1;
				}else if(total_score>=0 && total_score<50){
					grade_score = 0;
				}
				$("#grade_score_"+index).html(grade_score);
					
			});
		}

		function alignSize(){
			var content1Width = $("#row_0 .score_1").width();
			var content2Width = $("#row_0 .score_2").width();
			var content3Width = $("#row_0 .score_3").width();

			if(content1Width>=150) $(".table-header .item .score_1").width(content1Width);
			if(content1Width>=150) $(".table-header .item .score_2").width(content2Width);
			if(content1Width>=150) $(".table-header .item .score_3").width(content3Width);
		}

		//add column
		function add_column(index_score, min_score, max_score, score_text, score_percent){
			var length = $( "#row_0 .score_"+index_score+" input" ).length;
			$( ".table-content .item" ).each(function( index ) {
				var input_box = '<input type="text" class="inputbox" style="width: 20px;" name="n_score['+index+']['+(index_score-1)+']['+length+']" title="" data-placement="top" data-original-title="'+score_text+' ('+score_percent+'%)">';
				$("#row_"+index+" .score_"+index_score).append( input_box );
			});
			alignSize();
		}

		//remove coloum
		function remove_column(index_score, min_score, max_score){
			var length = $( "#row_0 .score_"+index_score+" input" ).length;
			if(length<=min_score) alert("min score box must more than "+min_score);
			
			$( ".table-content .item" ).each(function( index ) {
				if(length>min_score) $("#row_"+index+" .score_"+index_score).find(".inputbox").last().remove();
			});
			alignSize();
			summary_score();
		}

		
		//summary score each row when change value of input box
		$(document).on("change", ".inputbox", function(){
			summary_score();
		});

		//hightlight each rows
		$(document).on("focus", ".inputbox", function(){
			active_row(this);
		});

		
		// Handle add or remove column
		//btn score 1
		$(document).on("click", "#btn_score_1_add", function(){
			add_column(1, minScore1, maxScore1, "คะแนนเก็บ", percent1);
		});
		$(document).on("click", "#btn_score_1_remove", function(){
			remove_column(1, minScore1, maxScore1);
		});
		//btn score 2
		$(document).on("click","#btn_score_2_add", function(){
			add_column(2, minScore2, maxScore2, "คะแนนคุณธรรม", percent2);
		});
		$(document).on("click","#btn_score_2_remove", function(){
			remove_column(2, minScore2, maxScore2);
		});
		//btn score 3
		$(document).on("click","#btn_score_3_add", function(){
			add_column(3, minScore3, maxScore3, "คะแนนสอบ", percent3);
		});
		$(document).on("click","#btn_score_3_remove", function(){
			remove_column(3, minScore3, maxScore3);
		});
		
		//init document
		$(document).ready(function(){
			alignSize();
			summary_score();
		});
	</script>
</body>
</html>
