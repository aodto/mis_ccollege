<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$semester_id = $this->input->get("semester_id");
		if(empty($semester_id)) $semester_id = 1;
		
		$data = array();
		$data["header"] = $this->load->view("tmpl/header","", TRUE);
		$data["footer"] = $this->load->view("tmpl/footer","", TRUE);
		$data["topbar"] = $this->load->view("tmpl/topbar","", TRUE);
		$data["menu"] = $this->load->view("tmpl/menu","", TRUE);
		
		$query =$this->db->query("	SELECT g.id as group_id, g.code, g.subject_id, g.semester_id, s.code as subject_code, s.title as subject_title, s.amount_credit, s.amount_hours
									FROM  groups g  
									LEFT JOIN subjects s on(s.id=g.subject_id)
									WHERE g.published=1 AND g.semester_id={$semester_id} ");
		$items = $query->result();
		$data["items"] = $items;
		$data["save_score_form_id"] = 1;
		
		$this->load->view('index', $data);
	}
	public function edit_score()
	{
		$init_min_colomn = 4; //init number of min column
		$init_max_colomn = 20; //init number of max column
		
		$init_percent_1 = 60;//init percent of score1
		$init_percent_2 = 20;//init percent of score2
		$init_percent_3 = 20;//init percent of score3
		
		$subject_id 	= $this->input->get("subject_id");
		$form_id		= $this->input->get("form_id");
		$group_id		= $this->input->get("group_id");
		
		$data = array();
		$data["header"] = $this->load->view("tmpl/header","", TRUE);
		$data["footer"] = $this->load->view("tmpl/footer","", TRUE);
		$data["topbar"] = $this->load->view("tmpl/topbar","", TRUE);
		$data["menu"] = $this->load->view("tmpl/menu","", TRUE);
		
		$data["init_min_colomn"] = $init_min_colomn;
		$data["init_max_colomn"] = $init_max_colomn;
		
		$data["init_percent_1"] = $init_percent_1;
		$data["init_percent_2"] = $init_percent_2;
		$data["init_percent_3"] = $init_percent_3;

		//init number of column for input scores
		$data["init_score_1"] = $data["init_score_2"] = $data["init_score_3"] = array();
		for($i=0; $i<$init_min_colomn; $i++){
			array_push($data["init_score_1"], "");
			array_push($data["init_score_2"], "");
			array_push($data["init_score_3"], "");
		}
		
		//get all scores
		$query = $this->db->query("SELECT * FROM score_items WHERE form_id={$form_id} AND subject_id={$subject_id} ORDER BY student_id");
		$items_score = $query->result();
		$data["items_score"] = $items_score;
		
		//get all student who are register in this subject
		$query =$this->db->query("	SELECT regis.student_id, stu.code as student_code, stu.firstname, stu.lastname FROM subject_registration regis
									LEFT JOIN students stu ON(stu.id=regis.student_id) 
									WHERE regis.group_id={$group_id} AND regis.subject_id={$subject_id} AND stu.published=1 AND regis.published=1 ORDER BY student_id");
		$items_student = $query->result();
		$data["items_student"] = $items_student;
		
		
		$query = $this->db->query("SELECT * FROM subject_status WHERE published=1 ORDER BY ordering");
		$subject_status = $query->result();
		$data["subject_status"] = $subject_status;
		
		
		$data["form_id"] 	= $form_id;
		$data["subject_id"] = $subject_id;
		$data["group_id"] 	= $group_id;

		$this->load->view('edit_score', $data);
	}
	
	function view_log(){
		$form_id		= $this->input->get("form_id");
		$subject_id		= $this->input->get("subject_id");
		$group_id		= $this->input->get("group_id");
		
		$query = $this->db->query("SELECT * FROM score_logs WHERE form_id={$form_id} AND subject_id={$subject_id} AND group_id={$group_id} ORDER BY created_at DESC");
		
		foreach ($query->result() as $row)
		{
			echo "<pre>";
			echo "form_id: {$row->form_id}<br/>";
			echo "subject_id: {$row->subject_id}<br/>";
			echo "group_id: {$row->group_id}<br/>";
			echo "update_by: {$row->log_by}<br/>";
			echo "logs data:<br/>";
			print_r( json_decode($row->logs) );
			echo "<hr/>";
		}
		
	}

	function save_score(){
		$this->load->helper("datetime");

		$data 			= $this->input->post();
		$subject_id 	= $this->input->post("subject_id");
		$form_id		= $this->input->post("form_id");
		$group_id		= $this->input->post("group_id");

		$user_id 		= 100;
		$created_at 	= DateTimeHelper::getNow();
		$updated_at 	= DateTimeHelper::getNow();

		
		//clear old data and move to logs for monitoring
		$query = $this->db->query("SELECT * FROM score_items WHERE subject_id={$subject_id} AND group_id={$group_id} ORDER BY student_id");
		$log_data = json_encode( $query->result() );
		$data_log = new stdClass();
		$data_log->form_id = $form_id;
		$data_log->subject_id = $subject_id;
		$data_log->group_id = $group_id;
		$data_log->logs = $log_data;
		$data_log->log_by = $user_id;
		$data_log->created_at = $created_at;
		$this->db->insert("score_logs", $data_log);
		
		$this->db->delete('score_items', array('form_id' => $form_id));
		
		for($i=0; $i<count($data["n_score"]); $i++){
			//insert new data
			$data_insert 				= new stdClass();
			$data_insert->student_id	= $data["student_ids"][$i];
			$data_insert->score_1		= json_encode($data["n_score"][$i][0]);
			$data_insert->score_2		= json_encode($data["n_score"][$i][1]);;
			$data_insert->score_3		= json_encode($data["n_score"][$i][2]);
			$data_insert->status		= $data["score_status"][$i];
			$data_insert->created_at	= $created_at;
			$data_insert->updated_at	= $updated_at;
			$data_insert->updated_by	= $user_id;
			$data_insert->form_id		= $form_id;
			$data_insert->subject_id	= $subject_id;
			$data_insert->group_id		= $group_id;
				
			$this->db->insert("score_items", $data_insert);
		}
		
		$this->session->set_flashdata('status', "บันทึกข้อมูลเรียบร้อยแล้ว");
		
		redirect("home/edit_score?form_id={$form_id}&subject_id={$subject_id}&group_id={$group_id}");
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */